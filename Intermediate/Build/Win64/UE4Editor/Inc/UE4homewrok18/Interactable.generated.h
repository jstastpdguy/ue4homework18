// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4HOMEWROK18_Interactable_generated_h
#error "Interactable.generated.h already included, missing '#pragma once' in Interactable.h"
#endif
#define UE4HOMEWROK18_Interactable_generated_h

#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_SPARSE_DATA
#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_RPC_WRAPPERS
#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UE4HOMEWROK18_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UE4HOMEWROK18_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UE4HOMEWROK18_API UInteractable(UInteractable&&); \
	UE4HOMEWROK18_API UInteractable(const UInteractable&); \
public:


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	UE4HOMEWROK18_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	UE4HOMEWROK18_API UInteractable(UInteractable&&); \
	UE4HOMEWROK18_API UInteractable(const UInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(UE4HOMEWROK18_API, UInteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable)


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractable(); \
	friend struct Z_Construct_UClass_UInteractable_Statics; \
public: \
	DECLARE_CLASS(UInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/UE4homewrok18"), UE4HOMEWROK18_API) \
	DECLARE_SERIALIZER(UInteractable)


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_GENERATED_UINTERFACE_BODY() \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_GENERATED_UINTERFACE_BODY() \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_9_PROLOG
#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_RPC_WRAPPERS \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE4homewrok18_Source_UE4homewrok18_Interactable_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	UE4homewrok18_Source_UE4homewrok18_Interactable_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4HOMEWROK18_API UClass* StaticClass<class UInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UE4homewrok18_Source_UE4homewrok18_Interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
