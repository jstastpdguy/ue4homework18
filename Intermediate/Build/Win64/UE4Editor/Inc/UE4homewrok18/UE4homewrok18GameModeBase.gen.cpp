// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4homewrok18/UE4homewrok18GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUE4homewrok18GameModeBase() {}
// Cross Module References
	UE4HOMEWROK18_API UClass* Z_Construct_UClass_AUE4homewrok18GameModeBase_NoRegister();
	UE4HOMEWROK18_API UClass* Z_Construct_UClass_AUE4homewrok18GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_UE4homewrok18();
// End Cross Module References
	void AUE4homewrok18GameModeBase::StaticRegisterNativesAUE4homewrok18GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AUE4homewrok18GameModeBase_NoRegister()
	{
		return AUE4homewrok18GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4homewrok18,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "UE4homewrok18GameModeBase.h" },
		{ "ModuleRelativePath", "UE4homewrok18GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUE4homewrok18GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::ClassParams = {
		&AUE4homewrok18GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUE4homewrok18GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUE4homewrok18GameModeBase, 3980919953);
	template<> UE4HOMEWROK18_API UClass* StaticClass<AUE4homewrok18GameModeBase>()
	{
		return AUE4homewrok18GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUE4homewrok18GameModeBase(Z_Construct_UClass_AUE4homewrok18GameModeBase, &AUE4homewrok18GameModeBase::StaticClass, TEXT("/Script/UE4homewrok18"), TEXT("AUE4homewrok18GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUE4homewrok18GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
