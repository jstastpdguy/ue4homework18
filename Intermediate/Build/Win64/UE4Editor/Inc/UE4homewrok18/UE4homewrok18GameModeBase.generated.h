// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4HOMEWROK18_UE4homewrok18GameModeBase_generated_h
#error "UE4homewrok18GameModeBase.generated.h already included, missing '#pragma once' in UE4homewrok18GameModeBase.h"
#endif
#define UE4HOMEWROK18_UE4homewrok18GameModeBase_generated_h

#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_SPARSE_DATA
#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_RPC_WRAPPERS
#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUE4homewrok18GameModeBase(); \
	friend struct Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUE4homewrok18GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4homewrok18"), NO_API) \
	DECLARE_SERIALIZER(AUE4homewrok18GameModeBase)


#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUE4homewrok18GameModeBase(); \
	friend struct Z_Construct_UClass_AUE4homewrok18GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUE4homewrok18GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4homewrok18"), NO_API) \
	DECLARE_SERIALIZER(AUE4homewrok18GameModeBase)


#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUE4homewrok18GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUE4homewrok18GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUE4homewrok18GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUE4homewrok18GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUE4homewrok18GameModeBase(AUE4homewrok18GameModeBase&&); \
	NO_API AUE4homewrok18GameModeBase(const AUE4homewrok18GameModeBase&); \
public:


#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUE4homewrok18GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUE4homewrok18GameModeBase(AUE4homewrok18GameModeBase&&); \
	NO_API AUE4homewrok18GameModeBase(const AUE4homewrok18GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUE4homewrok18GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUE4homewrok18GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUE4homewrok18GameModeBase)


#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_12_PROLOG
#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_RPC_WRAPPERS \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_INCLASS \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4HOMEWROK18_API UClass* StaticClass<class AUE4homewrok18GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UE4homewrok18_Source_UE4homewrok18_UE4homewrok18GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
