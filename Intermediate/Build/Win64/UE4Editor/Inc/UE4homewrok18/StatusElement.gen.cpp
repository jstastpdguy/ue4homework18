// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4homewrok18/StatusElement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStatusElement() {}
// Cross Module References
	UE4HOMEWROK18_API UEnum* Z_Construct_UEnum_UE4homewrok18_EStatusEffect();
	UPackage* Z_Construct_UPackage__Script_UE4homewrok18();
	UE4HOMEWROK18_API UClass* Z_Construct_UClass_AStatusElement_NoRegister();
	UE4HOMEWROK18_API UClass* Z_Construct_UClass_AStatusElement();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	UE4HOMEWROK18_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	static UEnum* EStatusEffect_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UE4homewrok18_EStatusEffect, Z_Construct_UPackage__Script_UE4homewrok18(), TEXT("EStatusEffect"));
		}
		return Singleton;
	}
	template<> UE4HOMEWROK18_API UEnum* StaticEnum<EStatusEffect>()
	{
		return EStatusEffect_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EStatusEffect(EStatusEffect_StaticEnum, TEXT("/Script/UE4homewrok18"), TEXT("EStatusEffect"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UE4homewrok18_EStatusEffect_Hash() { return 1269856259U; }
	UEnum* Z_Construct_UEnum_UE4homewrok18_EStatusEffect()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UE4homewrok18();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EStatusEffect"), 0, Get_Z_Construct_UEnum_UE4homewrok18_EStatusEffect_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EStatusEffect::SPEEDUP", (int64)EStatusEffect::SPEEDUP },
				{ "EStatusEffect::SPEEDDOWN", (int64)EStatusEffect::SPEEDDOWN },
				{ "EStatusEffect::NORMAL", (int64)EStatusEffect::NORMAL },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "StatusElement.h" },
				{ "NORMAL.Name", "EStatusEffect::NORMAL" },
				{ "SPEEDDOWN.Name", "EStatusEffect::SPEEDDOWN" },
				{ "SPEEDUP.Name", "EStatusEffect::SPEEDUP" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UE4homewrok18,
				nullptr,
				"EStatusEffect",
				"EStatusEffect",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AStatusElement::execRandomStatus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EStatusEffect*)Z_Param__Result=P_THIS->RandomStatus();
		P_NATIVE_END;
	}
	void AStatusElement::StaticRegisterNativesAStatusElement()
	{
		UClass* Class = AStatusElement::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "RandomStatus", &AStatusElement::execRandomStatus },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AStatusElement_RandomStatus_Statics
	{
		struct StatusElement_eventRandomStatus_Parms
		{
			EStatusEffect ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(StatusElement_eventRandomStatus_Parms, ReturnValue), Z_Construct_UEnum_UE4homewrok18_EStatusEffect, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "StatusElement.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AStatusElement, nullptr, "RandomStatus", nullptr, nullptr, sizeof(StatusElement_eventRandomStatus_Parms), Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AStatusElement_RandomStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AStatusElement_RandomStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AStatusElement_NoRegister()
	{
		return AStatusElement::StaticClass();
	}
	struct Z_Construct_UClass_AStatusElement_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EffectType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EffectType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStatusElement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4homewrok18,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AStatusElement_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AStatusElement_RandomStatus, "RandomStatus" }, // 4116827184
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStatusElement_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "StatusElement.h" },
		{ "ModuleRelativePath", "StatusElement.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType_MetaData[] = {
		{ "Category", "StatusElement" },
		{ "ModuleRelativePath", "StatusElement.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType = { "EffectType", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AStatusElement, EffectType), Z_Construct_UEnum_UE4homewrok18_EStatusEffect, METADATA_PARAMS(Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStatusElement_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "StatusElement" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "StatusElement.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AStatusElement_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AStatusElement, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AStatusElement_Statics::NewProp_MeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AStatusElement_Statics::NewProp_MeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AStatusElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AStatusElement_Statics::NewProp_EffectType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AStatusElement_Statics::NewProp_MeshComponent,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AStatusElement_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AStatusElement, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStatusElement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStatusElement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStatusElement_Statics::ClassParams = {
		&AStatusElement::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AStatusElement_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AStatusElement_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AStatusElement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStatusElement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStatusElement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStatusElement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStatusElement, 2140339889);
	template<> UE4HOMEWROK18_API UClass* StaticClass<AStatusElement>()
	{
		return AStatusElement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStatusElement(Z_Construct_UClass_AStatusElement, &AStatusElement::StaticClass, TEXT("/Script/UE4homewrok18"), TEXT("AStatusElement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStatusElement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
