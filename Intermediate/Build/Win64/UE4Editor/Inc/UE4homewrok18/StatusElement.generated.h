// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EStatusEffect : uint8;
#ifdef UE4HOMEWROK18_StatusElement_generated_h
#error "StatusElement.generated.h already included, missing '#pragma once' in StatusElement.h"
#endif
#define UE4HOMEWROK18_StatusElement_generated_h

#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_SPARSE_DATA
#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRandomStatus);


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRandomStatus);


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStatusElement(); \
	friend struct Z_Construct_UClass_AStatusElement_Statics; \
public: \
	DECLARE_CLASS(AStatusElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4homewrok18"), NO_API) \
	DECLARE_SERIALIZER(AStatusElement) \
	virtual UObject* _getUObject() const override { return const_cast<AStatusElement*>(this); }


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAStatusElement(); \
	friend struct Z_Construct_UClass_AStatusElement_Statics; \
public: \
	DECLARE_CLASS(AStatusElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4homewrok18"), NO_API) \
	DECLARE_SERIALIZER(AStatusElement) \
	virtual UObject* _getUObject() const override { return const_cast<AStatusElement*>(this); }


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStatusElement(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStatusElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStatusElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStatusElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStatusElement(AStatusElement&&); \
	NO_API AStatusElement(const AStatusElement&); \
public:


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStatusElement(AStatusElement&&); \
	NO_API AStatusElement(const AStatusElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStatusElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStatusElement); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStatusElement)


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_PRIVATE_PROPERTY_OFFSET
#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_17_PROLOG
#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_PRIVATE_PROPERTY_OFFSET \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_RPC_WRAPPERS \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_INCLASS \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_PRIVATE_PROPERTY_OFFSET \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_SPARSE_DATA \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_INCLASS_NO_PURE_DECLS \
	UE4homewrok18_Source_UE4homewrok18_StatusElement_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4HOMEWROK18_API UClass* StaticClass<class AStatusElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UE4homewrok18_Source_UE4homewrok18_StatusElement_h


#define FOREACH_ENUM_ESTATUSEFFECT(op) \
	op(EStatusEffect::SPEEDUP) \
	op(EStatusEffect::SPEEDDOWN) \
	op(EStatusEffect::NORMAL) 

enum class EStatusEffect : uint8;
template<> UE4HOMEWROK18_API UEnum* StaticEnum<EStatusEffect>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
