// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StatusElement.h"
#include "Components/BoxComponent.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class UE4HOMEWROK18_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AStatusElement> BonusClass;

	UPROPERTY(BlueprintReadWrite)
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		AFood* FoodElement;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		EStatusEffect Status;

	UPROPERTY(BlueprintReadWrite)
		FVector BoundsExt;

	UPROPERTY(BlueprintReadWrite)
		TArray<UBoxComponent*> CollisionBoxes;
	
	UPROPERTY(BlueprintReadWrite)
		int32 Scores;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement( int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
		void Move(float newX = 0.f, float newY = 0.f);
	UFUNCTION()
		bool SpawnFood(int32 Min, int32 Max, bool Type = 0);
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION(BlueprintCallable)
		void ChangeSpeed(int32 multiplier);
	UFUNCTION(BlueprintCallable)
		void SpawnBonus();
};