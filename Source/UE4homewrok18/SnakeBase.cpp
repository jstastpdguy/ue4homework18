// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "StatusElement.h"
#include "Barrier.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/GameFramework/KillZVolume.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	Scores = 0;
	LastMoveDirection = EMovementDirection::DOWN;
	Status = EStatusEffect::NORMAL;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(1/MovementSpeed);
	AddSnakeElement();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FMath::RandRange(0, 100) < 3) 
		SpawnFood(0, CollisionBoxes.Num(), 1);
	if (!IsValid(FoodElement)) 
		SpawnFood(0, CollisionBoxes.Num());
	Move();
}

//Adds SnakeBaseElement to the end of the Snake
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	//If Snake has no elements, BaseElement spawn at the (0, 0) coordinates;
	FVector NewLocation(0,0,0);

	//Checks head movement direction to add new element to the tail
	for (int i = 0; i < ElementsNum; ++i) {
		if(SnakeElements.Num() == 1){
			
			//Head element location
			FVector LastLocation = SnakeElements.Last()->GetActorLocation();
			
			//switch on which direction moves head, to add new elemtn behind it
			switch (LastMoveDirection)
			{
			case EMovementDirection::UP:
				LastLocation.X -= ElementSize;
				NewLocation = LastLocation;
				break;
			case EMovementDirection::DOWN:
				LastLocation.X += ElementSize;
				NewLocation = LastLocation;
				break;
			case EMovementDirection::RIGHT:
				LastLocation.Y += ElementSize;
				NewLocation = LastLocation;
				break;
			case EMovementDirection::LEFT:
				LastLocation.Y -= ElementSize;
				NewLocation = LastLocation;
				break;
			default:
				break;
			}
		}
		/*Cheks tail element location and tail-1 for count movement direction of tail*/
		else if (SnakeElements.Num() > 1) {
			
			//tail element location
			FVector LastLocation = SnakeElements.Last()->GetActorLocation();

			//tail -1 element location
			FVector LocationMasc = LastLocation - SnakeElements[SnakeElements.Num() - 2]->GetActorLocation();
			
			//count new location of tail move on x coordinates
			
			if(LocationMasc.X){
				if (LocationMasc.X > 0) {
					LastLocation.X += ElementSize;
					NewLocation = LastLocation;
				}
				else {
					LastLocation.X -= ElementSize;
					NewLocation = LastLocation;
				}
			}
			//count new location of tail move on y coordinates
			
			else {
				if (LocationMasc.Y > 0) {
					LastLocation.Y += ElementSize;
					NewLocation = LastLocation;
				}
				else {
					LastLocation.Y -= ElementSize;
					NewLocation = LastLocation;
				}
			}
		}


		//make new transform with new location

		FTransform NewTransform(NewLocation);
		
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		NewSnakeElem->SnakeOwner = this;

		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);

		if (!ElementIndex) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move(float newX, float newY)
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection) {
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	if (newX || newY) { 
		if (newX) 
		{
			MovementVector.X -= newX;
			MovementVector.Y += SnakeElements[0]->GetActorLocation().Y;
		}
		else {
			MovementVector.X += SnakeElements[0]->GetActorLocation().X;
			MovementVector.Y -= newY;
		}
		SnakeElements[0]->SetActorLocation(MovementVector); 
	}
	else SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

/*void ASnakeBase::SpawnFood() {
	
	FVector Origin, Extent;
	float minX, minY, maxX, maxY;
	int32  OutX = 0, OutY = 0, Size = FMath::FloorToInt(ElementSize);
	
	SnakeElements[0]->GetActorBounds(false, Origin, Extent);

	minX = Origin.X - Extent.X;
	minY = Origin.Y - Extent.Y;
	maxX = Origin.X + Extent.X;
	maxY = Origin.Y + Extent.Y;

	for (int i = 1; i < SnakeElements.Num(); i++) {
		SnakeElements[i]->GetActorBounds(false, Origin, Extent);
		if ((Origin.X - Extent.X) < minX) minX = Origin.X - Extent.X;
		if ((Origin.Y - Extent.Y) < minX) minY = Origin.Y - Extent.Y;
		if ((Origin.X + Extent.X) < minX) maxX = Origin.X + Extent.X;
		if ((Origin.Y + Extent.Y) < minX) maxY = Origin.Y + Extent.Y;
	}

	switch (FMath::RandBool()) {
	case 0:
		OutX = FMath::RandRange(Size - BoundsExt.X, minX);
		break;
	case 1:
		OutX = FMath::RandRange(maxX, BoundsExt.X - Size );
		break;
	}

	switch (FMath::RandBool()) {
	case 0:
		OutY = FMath::RandRange(Size - BoundsExt.Y, minY);
		break;
	case 1:
		OutY = FMath::FRandRange(maxY, BoundsExt.Y - Size);
		break;
	}

	if(OutX % Size)
		if(OutX > 0)
			OutX += (Size - OutX % Size);
		else 
			OutX -= Size + OutX % Size;

	if(OutY % Size)
		if(OutY > 0)
			OutY += (Size - OutY % Size);
		else
			OutY -= Size + OutY % Size;

	FoodElement = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FVector(OutX,OutY, 0)));
}*/

/*Cheks overlapps collision boxes with snake and barrier and spawn food (if Type = 0, or StatusElement if = 1)
in empty space*/
bool ASnakeBase::SpawnFood(int32 Min, int32 Max, bool Type) {

	if (Min == Max) return false;

	int32 Index = FMath::RandRange(Min, Max - 1);
	TArray<AActor*> SnakeElement, Barrier;

	CollisionBoxes[Index]->GetOverlappingActors(SnakeElement, TSubclassOf<ASnakeElementBase>());
	CollisionBoxes[Index]->GetOverlappingActors(Barrier, TSubclassOf<ABarrier>());

	if ((!SnakeElement.Num() && !Barrier.Num())) {

		if (Type) {
			TArray<AActor*> Food;
			CollisionBoxes[Index]->GetOverlappingActors(Food, TSubclassOf<AFood>());

			if (!Food.Num()) {
				float X = CollisionBoxes[Index]->K2_GetComponentLocation().X, Y = CollisionBoxes[Index]->K2_GetComponentLocation().Y;
				GetWorld()->SpawnActor<AStatusElement>(BonusClass, FTransform(FVector(X, Y, 0)));
				return true;
			}
			else {
				SnakeElement.Empty();
				Barrier.Empty();
				Food.Empty();

				if (ASnakeBase::SpawnFood(Min, Index, Type)) return true;
				else if (ASnakeBase::SpawnFood(Index + 1, Max, Type)) return true;
				else return false;
			}

		}

		float X = CollisionBoxes[Index]->K2_GetComponentLocation().X, Y = CollisionBoxes[Index]->K2_GetComponentLocation().Y;

		FoodElement = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FVector(X, Y, 0)));
		return true;
	}
	else {
		SnakeElement.Empty();
		Barrier.Empty();

		if (ASnakeBase::SpawnFood(Min, Index)) return true;
		else if (ASnakeBase::SpawnFood(Index + 1, Max)) return true;
		else return false;
	}

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		
		//when overlap killZ snake's head move to reverse side of the map
		if (Cast<AKillZVolume>(Other)) { 
			Move(Other->GetActorLocation().X, Other->GetActorLocation().Y);
		}

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

//increase Movement speed by multiplier if positive, decrease if negative
void ASnakeBase::ChangeSpeed(int32 multiplier) {
	if (multiplier > 0) {
		MovementSpeed *= multiplier;
	}
	else
		MovementSpeed /= abs(multiplier);

	SetActorTickInterval(1 / MovementSpeed);
}

void ASnakeBase::SpawnBonus()
{
	FVector Origin, Extent;
	float minX, minY, maxX, maxY, OutX = 0, OutY = 0;;

	SnakeElements[0]->GetActorBounds(false, Origin, Extent);

	minX = Origin.X - Extent.X;
	minY = Origin.Y - Extent.Y;
	maxX = Origin.X + Extent.X;
	maxY = Origin.Y + Extent.Y;

	for (int i = 1; i < SnakeElements.Num(); i++) {
		SnakeElements[i]->GetActorBounds(false, Origin, Extent);
		if ((Origin.X - Extent.X) < minX) minX = Origin.X - Extent.X;
		if ((Origin.Y - Extent.Y) < minX) minY = Origin.Y - Extent.Y;
		if ((Origin.X + Extent.X) < minX) maxX = Origin.X + Extent.X;
		if ((Origin.Y + Extent.Y) < minX) maxY = Origin.Y + Extent.Y;
	}

	switch (FMath::RandBool()) {
	case 0:
		OutX = FMath::FRandRange(ElementSize - 500, minX);
		break;
	case 1:
		OutX = FMath::FRandRange(maxX, 500 - ElementSize);
		break;
	}

	switch (FMath::RandBool()) {
	case 0:
		OutY = FMath::FRandRange(ElementSize - 500, minY);
		break;
	case 1:
		OutY = FMath::FRandRange(maxY, 500 - ElementSize);
		break;
	}

	if (static_cast<int64>(OutX) % static_cast<int64>(ElementSize))
		OutX += ElementSize - static_cast<int64>(OutX) % static_cast<int>(ElementSize);

	if (static_cast<int64>(OutY) % static_cast<int>(ElementSize))
		OutY += ElementSize - static_cast<int64>(OutY) % static_cast<int>(ElementSize);

	GetWorld()->SpawnActor<AStatusElement>(BonusClass, FTransform(FVector(OutX, OutY, 0)));
}
