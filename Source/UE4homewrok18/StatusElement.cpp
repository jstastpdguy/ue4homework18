// Add status effects to snake when overlapped
#include "StatusElement.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AStatusElement::AStatusElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->InitialLifeSpan = 10.f;
	EffectType = RandomStatus();
}

// Called when the game starts or when spawned
void AStatusElement::BeginPlay()
{
	Super::BeginPlay();
	
}

EStatusEffect AStatusElement::RandomStatus()
{
	switch (FMath::RandRange(0, 1)) {
	case 0:
		return EStatusEffect::SPEEDUP;
	case 1:
		return EStatusEffect::SPEEDDOWN;
	}
	return EStatusEffect::NORMAL;
}

// Called every frame
void AStatusElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStatusElement::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {

		auto Snake = Cast<ASnakeBase>(Interactor);
		Destroy();

		if (IsValid(Snake)) {
			switch (EffectType) {
			case EStatusEffect::SPEEDDOWN:
				if (Snake->Status == EStatusEffect::SPEEDUP) {
					Snake->Status = EStatusEffect::NORMAL;
					Snake->ChangeSpeed(-2);
				}
				else if (Snake->Status == EStatusEffect::NORMAL) {
					Snake->Status = EStatusEffect::SPEEDDOWN;
					Snake->ChangeSpeed(-2);
				}

				break;

			case EStatusEffect::SPEEDUP:
				if (Snake->Status == EStatusEffect::SPEEDDOWN) {
					Snake->Status = EStatusEffect::NORMAL;
					Snake->ChangeSpeed(2);
				}
				else if (Snake->Status == EStatusEffect::NORMAL) {
					Snake->Status = EStatusEffect::SPEEDUP;
					Snake->ChangeSpeed(2);
				}
				break;
			}
		}
	}
}