// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "StatusElement.generated.h"

UENUM(BlueprintType)
enum class EStatusEffect : uint8 {
	SPEEDUP,
	SPEEDDOWN,
	NORMAL
};

UCLASS()
class UE4HOMEWROK18_API AStatusElement : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStatusElement();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		EStatusEffect EffectType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UFUNCTION()
		EStatusEffect RandomStatus();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};